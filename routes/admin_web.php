<?php

use App\Http\Controllers\AboutMeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BioController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\TestimonialController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminHomeController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\PortfolioTypeController;

//uncomment below line to create new user by accessing the below route. Make sure to comment/remove below code
//once user is created!
//Route::get('/createAdminUser', function () {
//    $user = \Illuminate\Support\Facades\DB::table("users")->insert([
//        "name"=>'wahed', //change this to your preferred name
//        "email"=>'hi@waheduzzaman.com', //change this to your own email
//        "password"=>bcrypt("123123123"), //change this password
//    ]);
//    return $user;
//});



Route::get('admin/login', function () {return view('auth.login');})->name("login");
Route::post('admin/login', [LoginController::class, 'login']);

Route::group(['middleware' => ['auth'],'prefix'=>'admin'], function () {

    Route::post('logout', [LoginController::class, 'logout'])->name("logout");

    Route::get('home', [AdminHomeController::class, 'index']);

    Route::resource('socialLinks',LinkController::class);
    Route::resource('about',AboutMeController::class);
    Route::resource('skills',SkillController::class);
    Route::resource('bio',BioController::class);
    Route::resource('testimonial',TestimonialController::class);
    Route::resource('portfolio',PortfolioController::class);
    Route::resource('portfolioType',PortfolioTypeController::class);
    Route::resource('organization',OrganizationController::class);
    Route::resource('experience',ExperienceController::class);

//    Route::get('about', [AboutMeController::class, 'index']);
    // Route::get('bio', [BioController::class, 'index']);
    // Route::get('experiences', [ExperienceController::class, 'index']);
    // Route::get('portfolio/types', [PortfolioTypeController::class, 'index']);
    // Route::get('portfolios', [PortfolioController::class, 'index']);
    // Route::get('skills', [SkillController::class, 'index']);
    // Route::get('testimonials', [TestimonialController::class, 'index']);

    // Route::post('bio', [BioController::class, 'store']);
    // Route::post('experiences', [ExperienceController::class, 'store']);
    // Route::post('portfolio/types', [PortfolioTypeController::class, 'store']);
    // Route::post('portfolios', [PortfolioController::class, 'store']);
    // Route::post('skills', [SkillController::class, 'store']);
    // Route::post('testimonials', [TestimonialController::class, 'store']);

    // Route::put('bio', [BioController::class, 'update']);
    // Route::put('experiences', [ExperienceController::class, 'update']);
    // Route::put('portfolio/types', [PortfolioTypeController::class, 'update']);
    // Route::put('portfolios', [PortfolioController::class, 'update']);
    // Route::put('skills', [SkillController::class, 'update']);
    // Route::put('testimonials', [TestimonialController::class, 'update']);


    // Route::delete('bio', [BioController::class, 'destroy']);
    // Route::delete('experiences', [ExperienceController::class, 'destroy']);
    // Route::delete('portfolio/types', [PortfolioTypeController::class, 'destroy']);
    // Route::delete('portfolios', [PortfolioController::class, 'destroy']);
    // Route::delete('skills', [SkillController::class, 'destroy']);
    // Route::delete('testimonials', [TestimonialController::class, 'destroy']);


});
