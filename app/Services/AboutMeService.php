<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 6/6/22
 * Time: 9:38 PM
 */

namespace App\Services;

use App\Models\AboutMe;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;

class AboutMeService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return AboutMe::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $existingAboutMe = AboutMe::find($formRequest->get("id"));

        if ($formRequest->get("name") != null)
            $existingAboutMe->name = $formRequest->get("name");
        if ($formRequest->get("designation") != null)
            $existingAboutMe->designation = $formRequest->get("designation");
        if ($formRequest->get("punchline") != null)
            $existingAboutMe->punchline = $formRequest->get("punchline");
        if ($formRequest->get("short_description") != null)
            $existingAboutMe->short_description = $formRequest->get("short_description");
        if ($formRequest->get("main_bg_image") != null) {
            $this->deleteImage(public_path($existingAboutMe->main_bg_image));
            $existingAboutMe->main_bg_image = $formRequest->get("main_bg_image");
        }
        if ($formRequest->get("about_image") != null) {
            $existingAboutMe->about_image = $formRequest->get("about_image");
        }
        if ($formRequest->get("current_org") != null) {
            $existingAboutMe->current_org = $formRequest->get("current_org");
        }

        return $existingAboutMe->update();
    }

    public function delete($object)
    {
        // TODO: Implement delete() method.
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        if ($formRequest->main_bg_img != null) {
            $mainBgName = $this->processImage($formRequest, "main_bg_img");
            $formRequest->merge(["main_bg_image" => '/img/' . $mainBgName]);
        }
        if ($formRequest->about_img != null) {
            $aboutImgName = $this->processImage($formRequest, "about_img");
            $formRequest->merge(["about_image" => '/img/' . $aboutImgName]);
        }
        try {
            if ($formRequest->id == null)
                $this->create($formRequest);
            else
                $this->update(null, $formRequest);
            return true;
        } catch (Exception $ex) {
            //todo:: Slack the exception
            return false;
        }
    }

    private function processImage(FormRequest $formRequest, $file_name)
    {
        $imageName = $formRequest->$file_name->getClientOriginalName();
        $formRequest->$file_name->move(public_path('img'), $imageName);
        return $imageName;
    }

    private function deleteImage($imagePath)
    {
        if (file_exists($imagePath)) {
            File::delete($imagePath);
        }
    }
}
