<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: me@waheduzzaman.com
 * Date: 27/3/22
 * Time: 6:07 PM
 */

namespace App\Services;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseService
{
    public abstract function create(FormRequest $formRequest);
    public abstract function createOrUpdate(FormRequest $formRequest);
    public abstract function update($object, FormRequest $formRequest);
    public abstract function delete($object);
    public abstract function get();
    public abstract function getAll();
}
