<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 11:31 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\PortfolioType;
use Illuminate\Foundation\Http\FormRequest;

class PortfolioTypeService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return PortfolioType::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $socialLink = $object;
        if ($formRequest->get("title") != null)
            $socialLink->title = $formRequest->get("title");
        return $socialLink->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof PortfolioType) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        return PortfolioType::all();
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
