<?php

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Organization;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return Organization::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $organization = $object;
        if ($formRequest->get("website") != null)
            $organization->website = $formRequest->get("website");
        if ($formRequest->get("short_description") != null)
            $organization->short_description = $formRequest->get("short_description");
        if ($formRequest->get("address") != null)
            $organization->address = $formRequest->get("address");
        if ($formRequest->get("started_at") != null)
            $organization->started_at = $formRequest->get("started_at");
        if ($formRequest->get("ended_at") != null)
            $organization->ended_at = $formRequest->get("ended_at");
        if ($formRequest->get("company_name") != null)
            $organization->company_name = $formRequest->get("company_name");
        return $organization->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Organization) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        return Organization::orderBy('id','desc')->get();
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
