<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 11:48 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Skill;
use Illuminate\Foundation\Http\FormRequest;

class SkillService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return Skill::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $skill = $object;
        if ($formRequest->get("key") != null)
            $skill->key = $formRequest->get("key");
        if ($formRequest->get("value") != null)
            $skill->value = $formRequest->get("value");
        return $skill->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Skill) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
