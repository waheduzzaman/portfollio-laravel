<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 28/3/22
 * Time: 8:19 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Experience;
use Illuminate\Foundation\Http\FormRequest;
class ExperienceService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return Experience::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $experience = $object;
        if ($formRequest->get("project_name") != null)
            $experience->project_name = $formRequest->get("project_name");
        if ($formRequest->get("url") != null)
            $experience->url = $formRequest->get("url");
        if ($formRequest->get("project_date") != null)
            $experience->project_date = $formRequest->get("project_date");
        if ($formRequest->get("period") != null)
            $experience->period = $formRequest->get("period");
        if ($formRequest->get("organization_fk") != null)
            $experience->organization_fk = $formRequest->get("organization_fk");
        if ($formRequest->get("text") != null)
            $experience->text = $formRequest->get("text");
        return $experience->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Experience) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        return Experience::orderBy('id','desc')->get();
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
