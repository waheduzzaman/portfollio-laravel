<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 28/3/22
 * Time: 8:19 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Portfolio;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class PortfolioService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        if ($formRequest->hasFile('images')) {
            $this->processImage($formRequest);
        }
        return Portfolio::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        if ($formRequest->hasFile('images')) {
            //todo:: delete previously stored images
            $this->processImage($formRequest);
        }

        $socialLink = $object;
        if ($formRequest->get("title") != null)
            $socialLink->title = $formRequest->get("title");
        if ($formRequest->get("project_link") != null)
            $socialLink->project_link = $formRequest->get("project_link");
        if ($formRequest->get("project_date") != null)
            $socialLink->project_date = $formRequest->get("project_date");
        if ($formRequest->get("description") != null)
            $socialLink->description = $formRequest->get("description");
        if ($formRequest->get("portfolio_type_fk") != null)
            $socialLink->portfolio_type_fk = $formRequest->get("portfolio_type_fk");
        if ($formRequest->get("image_urls") != null)
            $socialLink->image_urls = $formRequest->get("image_urls");
        return $socialLink->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Portfolio) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        return Portfolio::all();
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }

    private function processImage(FormRequest $request)
    {
        $imageArray = [];
        $allowedfileExtension = ['jpg', 'png', 'jpeg', 'gif','PNG', 'JPG', 'JPEG'];
        $files = $request->file('images');
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                $imageName = $file->getClientOriginalName();

                $image = Image::make($file->getRealPath());
                $image->resize(800, 600)->save(public_path('/img/portfolio/') . $imageName);

                $imageArray[] = '/img/portfolio/' . $imageName;
            }
        }
        if (count($imageArray) > 0) {
            $request->merge(["image_urls" => json_encode($imageArray,JSON_UNESCAPED_SLASHES)]);
        }
    }
}
