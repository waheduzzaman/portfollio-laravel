<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: me@waheduzzaman.com
 * Date: 26/3/22
 * Time: 11:31 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Link;
use Illuminate\Foundation\Http\FormRequest;

class LinkService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return Link::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $socialLink = $object;
        if ($formRequest->get("title") != null)
            $socialLink->title = $formRequest->get("title");
        if ($formRequest->get("url") != null)
            $socialLink->url = $formRequest->get("url");
        if ($formRequest->get("bootstrap_icon_name") != null)
            $socialLink->bootstrap_icon_name = $formRequest->get("bootstrap_icon_name");
        return $socialLink->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Link) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
