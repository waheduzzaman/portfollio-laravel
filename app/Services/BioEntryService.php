<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 28/3/22
 * Time: 11:48 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Bio;
use Illuminate\Foundation\Http\FormRequest;

class BioEntryService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        return Bio::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $bio = $object;
        if ($formRequest->get("key") != null)
            $bio->key = $formRequest->get("key");
        if ($formRequest->get("value") != null)
            $bio->value = $formRequest->get("value");
        return $bio->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Bio) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }
}
