<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/7/22
 * Time: 11:48 PM
 */

namespace App\Services;

use App\Exceptions\ClassCastException;
use App\Models\Testimonial;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class TestimonialService extends BaseService
{

    public function create(FormRequest $formRequest)
    {
        if ($formRequest->client_img != null) {
            $client_img = $this->processImage($formRequest, "client_img");
            $formRequest->merge(["client_photo" => '/img/testimonials/' . $client_img]);
        }
        return Testimonial::create($formRequest->all());
    }

    public function update($object, FormRequest $formRequest)
    {
        $testimonial = $object;

        if ($formRequest->client_img != null) {
            $client_img = $this->processImage($formRequest, "client_img");
            $formRequest->merge(["client_photo" => '/img/testimonials/' . $client_img]);
        }

        if ($formRequest->get("client_name") != null)
            $testimonial->client_name = $formRequest->get("client_name");
        if ($formRequest->get("client_photo") != null)
            $testimonial->client_photo = $formRequest->get("client_photo");
        if ($formRequest->get("client_designation") != null)
            $testimonial->client_designation = $formRequest->get("client_designation");
        if ($formRequest->get("client_organization_name") != null)
            $testimonial->client_organization_name = $formRequest->get("client_organization_name");
        if ($formRequest->get("client_organization_link") != null)
            $testimonial->client_organization_link = $formRequest->get("client_organization_link");
        if ($formRequest->get("testaments") != null)
            $testimonial->testaments = $formRequest->get("testaments");
        return $testimonial->save();
    }

    /**
     * @throws ClassCastException
     * unnecessary but shows how to use custom exception
     */
    public function delete($object)
    {
        if ($object instanceof Testimonial) {
            return $object->delete();
        } else {
            throw new ClassCastException();
        }
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function createOrUpdate(FormRequest $formRequest)
    {
        // TODO: Implement CreateOrUpdate() method.
    }

    private function processImage(FormRequest $formRequest, $file_name)
    {
        $imageFile = $formRequest->$file_name;
        $imageName = $imageFile->getClientOriginalName();

        $image = Image::make($imageFile->getRealPath());
        $image->resize(400, 400)->save(public_path('/img/testimonials/').$imageName);

        return $imageName;
    }
}
