<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * very basic implementation of, is string a valid bootstrap icon
 * ideally, we should have a regex check or a key value mapping with actual bootstrap icon set.
 * but, this will do the work for now!
 */

class ValidBootstrapIcon implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //does not contain any space &&
        //starts with bi
        return !str_contains($value," ") && str_starts_with($value,"bi") && strlen($value)>3;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Provided bootstrap icon is not valid';
    }
}
