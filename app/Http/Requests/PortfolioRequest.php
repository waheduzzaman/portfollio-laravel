<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/2/22
 * Time: 7:59 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class PortfolioRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'title' => 'required|max:90',
            "project_link" => 'required',
            "description" => 'required',
            "portfolio_type_fk" => 'required',
        ];
    }

    public function messages()
    {
        return [
            "title.required" => "Title is required",
            "title.max" => "maximum character is limited to 90",
        ];
    }

}
