<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 8:13 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

class SkillRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'key' => [
                Rule::unique('skills')->ignore($request->get('id')),
                'required'
            ],
            'value' => 'integer|digits_between:0,100|required',
        ];
    }

    public function messages()
    {
        return [
            "key.required" => "Skill Title is required",
            "key.unique" => "Skill Title is already in database record",
            "value.required" => "Skill value is required",
        ];
    }
}
