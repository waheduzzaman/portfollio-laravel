<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 7:59 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class PortfolioTypeRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'title' => 'required|max:90',
        ];
    }

    public function messages()
    {
        return [
            "title.required" => "Title is required",
            "title.max" => "maximum character is limited to 90",
        ];
    }

}
