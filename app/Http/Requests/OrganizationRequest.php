<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/2/22
 * Time: 7:59 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class OrganizationRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'company_name' => 'required|max:90',
            "started_at" => 'required|max:90',
            "ended_at" => 'max:90',
            "address" => 'required|max:90',
            "short_description" => 'required|max:120',
            "website" => 'max:90',
        ];
    }

    public function messages()
    {
        return [
            "company_name.required" => "company name is required",
            "company_name.max" => "maximum character for company name is limited to 90",

            "started_at.required" => "started at is required",
            "started_at.max" => "maximum character for started at is limited to 90",
  
            "address.required" => "address is required",
            "address.max" => "maximum character for address is limited to 90",
            
            "short_description.required" => "Company Details is required",
            "short_description.max" => "Company Details maximum character is limited to 120",
            
            "ended_at.max" => "maximum character for Left at is limited to 90",
            
            "website.max" => "website maximum character is limited to 90",
        ];
    }

}
