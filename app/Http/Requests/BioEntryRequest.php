<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 8:13 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

class BioEntryRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'key' => [
                Rule::unique('bios')->ignore($request->get('id')),
                'required'
            ],
            'value' => 'required',
        ];
    }

    public function messages()
    {
        return [
            "key.required" => "Bio Title is required",
            "key.unique" => "Bio Title is already present in database record",
            "value.required" => "Bio value is required",
        ];
    }
}
