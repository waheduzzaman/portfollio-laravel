<?php

/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/7/22
 * Time: 8:13 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

class TestimonialReqeust extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'client_name'=> [
                Rule::unique('testimonials')->ignore($request->get('id')),
                'required',
                'max:90',
            ],
            'client_designation' => 'required|max:90',
            'client_organization_name' => 'required|max:90',
            'client_organization_link' => 'max:120',
            'testaments' => 'required|max:290',
            'client_photo' => 'nullable|mimes:jpeg,jpg,png,gif|max:1024',
        ];
    }

    public function messages()
    {
        return [
            "client_name.required" => "Client Name is required",
            "client_name.unique" => "Client Name is already in database record",
            "client_name.max" => "Max character limit for Client Name is 90",

            "client_designation.required" => "Client Organization Designation is required",
            "client_designation.max" => "Max character limit for Client Organization Designation is 90",

            "client_organization_name.required" => "Client Organization Name is required",
            "client_organization_name.max" => "Max character limit for Client Organization Name is 90",

            "client_organization_link.max" => "Max character limit for Client Organization Website is 120",

            "testaments.required" => "Testament value is required",
            "testaments.max" => "Max character limit for Testament is 290",

            "client_photo.mimes" => "Client image type is not supported",
            "client_photo.max" => "Max photo size is 1MB",
        ];
    }
}
