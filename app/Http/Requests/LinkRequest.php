<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 26/3/22
 * Time: 7:59 PM
 */

namespace App\Http\Requests;

use App\Rules\ValidBootstrapIcon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

class LinkRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'title' => 'required|max:20',
            'url' => [
                Rule::unique('links')->ignore($request->get('id')),
                'required'
            ],
            'bootstrap_icon_name' => ['required','max:30', new ValidBootstrapIcon()],
        ];
    }

    public function messages()
    {
        return [
            "title.required" => "Social Site Name is required",
            "url.required" => "Profile link is required",
            "url.unique" => "Profile link is already in database record",
            "bootstrap_icon_name.required" => "Bootstrap icon for social site is missing",
        ];
    }

}
