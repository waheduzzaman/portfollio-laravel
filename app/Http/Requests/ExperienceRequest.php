<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/2/22
 * Time: 7:59 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class ExperienceRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            "text" => 'required|max:290',
            "project_name" => 'max:90',
            "url" => 'max:350',
            "period" => 'max:40',
            'organization_fk' => 'exists:organizations,id',
        ];
    }

    public function messages()
    {
        return [
            "text.required" => "text is required",
            "text.max" => "maximum character for text is limited to 90",

            "project_name.required" => "project name at is required",
            "project_name.max" => "maximum character for project name is limited to 90",
  
            "url.max" => "maximum character for url is limited to 350",
            
            "period.max" => "maximum character for period is limited to 120",
            
            "organization_fk.exists" => "Selected Organization does not exists!",
        ];
    }

}
