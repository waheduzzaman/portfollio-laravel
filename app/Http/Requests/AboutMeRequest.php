<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 6/6/22
 * Time: 9:41 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class AboutMeRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'name' => 'required|max:40',
            'short_description' => 'required|max:1000',
            'punchline' => 'required|max:450',
            'main_bg_img' => 'nullable|mimes:jpeg,jpg,png,gif|max:1024',
            'about_img' => 'nullable|mimes:jpeg,jpg,png,gif|max:1024',
        ];
    }

    public function messages()
    {
        return [
            "name.required" => "You must have a name! Right?",
            "short_description.required" => "Let the world know something about you!",
            "short_description.max" => "Max character limit for short description is 1000",
            "punchline.required" => "Throw us something punchy!",
            "punchline.max" => "Max character limit for punchline is 450",
            "main_bg_img.mimes" => "Uploaded image type is not supported",
            "about_img.mimes" => "Uploaded image type is not supported",
            "main_bg_img.max" => "Max file size is 1 MB",
            "about_img.max" => "Max file size is 1 MB",
        ];
    }

}
