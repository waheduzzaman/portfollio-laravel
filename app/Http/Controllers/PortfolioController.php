<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\PortfolioRequest;
use App\Models\Portfolio;
use App\Services\PortfolioService;
use App\Services\PortfolioTypeService;

class PortfolioController extends Controller
{

    private PortfolioService $portfolioService;
    private PortfolioTypeService $portfolioTypeService;

    public function __construct()
    {
        $this->portfolioTypeService = new PortfolioTypeService();
        $this->portfolioService = new PortfolioService();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.portfolio.portfolioList")->with("portfolios", $this->portfolioService->getAll())->with("navSelected", 9);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.portfolio.addPortfolio")
        ->with("types", $this->portfolioTypeService->getAll())
        ->with("navSelected", 9);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortfolioRequest $request)
    {
        if ($this->portfolioService->create($request))
            return redirect('/admin/portfolio')->with("successMessage", "New Portfolio Added");
        else
            return redirect('/admin/portfolio')->with("errorMessage", "Error while saving!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(Portfolio $portfolio)
    {
        return view("admin.portfolio.editPortfolio")
        ->with("portfolio", $portfolio)
        ->with("types", $this->portfolioTypeService->getAll())
        ->with("navSelected", 9);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioRequest $request, Portfolio $portfolio)
    {
        if ($this->portfolioService->update($portfolio, $request)) {
            return redirect('/admin/portfolio')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/portfolio')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        try {
            if ($this->portfolioService->delete($portfolio))
                return redirect('/admin/portfolio')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/portfolio')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
