<?php

namespace App\Http\Controllers;

use App\Models\AboutMe;
use App\Models\Bio;
use App\Models\Experience;
use App\Models\Link;
use App\Models\Organization;
use App\Models\Portfolio;
use App\Models\PortfolioType;
use App\Models\Skill;
use App\Models\Testimonial;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.app')
            ->with("aboutMe", AboutMe::find(1))
            ->with("links", Link::all())
            ->with("skills", Skill::all())
            ->with("bios", Bio::all())
            ->with("testimonials", Testimonial::all())
            ->with("portfolioTypes", PortfolioType::all())
            ->with("portfolios", Portfolio::all())
            ->with("organizations", Organization::orderBy('id','desc')->get())
            ->with("experiences", Experience::orderBy('id','desc')->get());
    }

    public function getPortfolioDetails(Portfolio $portfolio)
    {
        return view('layouts.portfolio_details')->with("portfolio", $portfolio);
    }
}
