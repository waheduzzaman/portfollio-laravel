<?php

namespace App\Http\Controllers;

use App\Models\PortfolioType;

use App\Exceptions\ClassCastException;
use App\Http\Requests\PortfolioTypeRequest;
use App\Services\PortfolioTypeService;

class PortfolioTypeController extends Controller
{
    private PortfolioTypeService $portfolioTypeService;

    public function __construct()
    {
        $this->portfolioTypeService = new PortfolioTypeService();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.portfolioType.portfolioTypeList")->with("portfolioTypes", PortfolioType::all())->with("navSelected", 8);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.portfolioType.createPortfolioType")->with("navSelected", 8);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortfolioTypeRequest $request)
    {
        if ($this->portfolioTypeService->create($request))
            return redirect('/admin/portfolioType')->with("successMessage", "New Portfolio Type Added");
        else
            return redirect('/admin/portfolioType')->with("errorMessage", "Error while saving!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(PortfolioType $portfolioType)
    {
        return view("admin.portfolioType.editPortfolioType")->with("portfolioType", $portfolioType)->with("navSelected", 8);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioTypeRequest $request, PortfolioType $portfolioType)
    {
        if ($this->portfolioTypeService->update($portfolioType, $request)) {
            return redirect('/admin/portfolioType')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/portfolioType')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(PortfolioType $portfolioType)
    {
        try {
            if ($this->portfolioTypeService->delete($portfolioType))
                return redirect('/admin/portfolioType')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/portfolioType')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
