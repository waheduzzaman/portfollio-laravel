<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\ExperienceRequest;
use App\Models\Experience;
use App\Models\Organization;
use App\Services\ExperienceService;

class ExperienceController extends Controller
{

    private ExperienceService $experienceService;

    public function __construct()
    {
        $this->experienceService = new ExperienceService();
    }

    public function index()
    {
        return view('admin.experiences.experienceList')->with('experiences', $this->experienceService->getAll())->with("navSelected", 10);;
    }


    public function create()
    {
        return view("admin.experiences.addExperience")
            ->with('organizations', Organization::all())
            ->with("navSelected", 10);
    }


    public function store(ExperienceRequest $request)
    {
        if ($this->experienceService->create($request))
            return redirect('/admin/experience')->with("successMessage", "New Experience Added");
        else
            return redirect('/admin/experience')->with("errorMessage", "Error while saving!");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience)
    {
        return view("admin.experiences.editExperience")
            ->with('experience', $experience)
            ->with('organizations', Organization::all())
            ->with("navSelected", 10);
    }


    public function update(ExperienceRequest $request, Experience $experience)
    {
        if ($this->experienceService->update($experience, $request))
            return redirect('/admin/experience')->with("successMessage", "Experience Updated!");
        else
            return redirect('/admin/experience')->with("errorMessage", "Error while updating!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience)
    {
        try {
            if ($this->experienceService->delete($experience))
                return redirect('/admin/experience')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/experience')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
