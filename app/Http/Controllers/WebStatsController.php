<?php

namespace App\Http\Controllers;

use App\Models\WebStats;
use Illuminate\Http\Request;

class WebStatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebStats  $webStats
     * @return \Illuminate\Http\Response
     */
    public function show(WebStats $webStats)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebStats  $webStats
     * @return \Illuminate\Http\Response
     */
    public function edit(WebStats $webStats)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebStats  $webStats
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebStats $webStats)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebStats  $webStats
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebStats $webStats)
    {
        //
    }
}
