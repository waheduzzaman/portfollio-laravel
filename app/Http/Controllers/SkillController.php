<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\SkillRequest;
use App\Models\Skill;
use App\Services\SkillService;

class SkillController extends Controller
{
    private SkillService $skillService;

    public function __construct()
    {
        $this->skillService = new SkillService();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.skills.skillList")->with("skills", Skill::all())->with("navSelected", 4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.skills.createSkill")->with("navSelected", 4);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SkillRequest $request)
    {
        if ($this->skillService->create($request))
            return redirect('/admin/skills')->with("successMessage", "New Skill Added");
        else
            return redirect('/admin/skills')->with("errorMessage", "Error while saving!");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function edit(Skill $skill)
    {
        return view("admin.skills.editSkill")->with("skill", $skill)->with("navSelected",4);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(SkillRequest $request, Skill $skill)
    {
        if ($this->skillService->update($skill, $request)) {
            return redirect('/admin/skills')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/skills')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        try {
            if ($this->skillService->delete($skill))
                return redirect('/admin/skills')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/skills')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
