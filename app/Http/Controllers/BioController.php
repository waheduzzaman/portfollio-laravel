<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\BioEntryRequest;
use App\Models\Bio;
use App\Services\BioEntryService;

class BioController extends Controller
{

    private BioEntryService $bioEntryService;

    public function __construct()
    {
        $this->bioEntryService = new BioEntryService();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.bio.bioList")->with("bios", Bio::all())->with("navSelected", 6);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.bio.addNewBioEntry")->with("navSelected", 6);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BioEntryRequest $request)
    {
        if ($this->bioEntryService->create($request))
            return redirect('/admin/bio')->with("successMessage", "New Bio Entry Added");
        else
            return redirect('/admin/bio')->with("errorMessage", "Error while saving!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bio  $bio
     * @return \Illuminate\Http\Response
     */
    public function edit(Bio $bio)
    {
        return view("admin.bio.editBio")->with("bio", $bio)->with("navSelected",6);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bio  $bio
     * @return \Illuminate\Http\Response
     */
    public function update(BioEntryRequest $request, Bio $bio)
    {
        if ($this->bioEntryService->update($bio, $request)) {
            return redirect('/admin/bio')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/bio')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bio  $bio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bio $bio)
    {
        try {
            if ($this->bioEntryService->delete($bio))
                return redirect('/admin/bio')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/bio')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
