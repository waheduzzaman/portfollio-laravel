<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\OrganizationRequest;
use App\Models\Organization;
use App\Services\OrganizationService;

class OrganizationController extends Controller
{
    private OrganizationService $organizationService;

    public function __construct()
    {
        $this->organizationService = new OrganizationService();
    }


    public function index()
    {
        return view('admin.organizations.organizationList')->with('organizations', $this->organizationService->getAll())->with("navSelected", 7);;
    }


    public function create()
    {
        return view("admin.organizations.addOrganization")
            ->with("navSelected", 7);
    }


    public function store(OrganizationRequest $request)
    {
        if ($this->organizationService->create($request))
            return redirect('/admin/organization')->with("successMessage", "New Organization Added");
        else
            return redirect('/admin/organization')->with("errorMessage", "Error while saving!");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return view("admin.organizations.editOrganization")
            ->with('organization', $organization)
            ->with("navSelected", 7);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationRequest $request, Organization $organization)
    {
        if ($this->organizationService->update($organization, $request))
            return redirect('/admin/organization')->with("successMessage", "Organization Updated!");
        else
            return redirect('/admin/organization')->with("errorMessage", "Error while updating!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        try {
            if ($this->organizationService->delete($organization))
                return redirect('/admin/organization')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/organization')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
