<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\TestimonialReqeust;
use App\Models\Testimonial;
use App\Services\TestimonialService;

class TestimonialController extends Controller
{
    private TestimonialService $testimonialService;

    public function __construct()
    {
        $this->testimonialService = new TestimonialService();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.testimonials.testimonialsList")->with("testimonials", Testimonial::all())->with("navSelected", 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.testimonials.addTestimonials")->with("navSelected", 5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestimonialReqeust $request)
    {
        if ($this->testimonialService->create($request))
            return redirect('/admin/testimonial')->with("successMessage", "New Testimonial Added");
        else
            return redirect('/admin/testimonial')->with("errorMessage", "Error while saving!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view("admin.testimonials.editTestimonial")->with("testimonial", $testimonial)->with("navSelected",5);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(TestimonialReqeust $request, Testimonial $testimonial)
    {
        if ($this->testimonialService->update($testimonial, $request)) {
            return redirect('/admin/testimonial')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/testimonial')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        try {
            if ($this->testimonialService->delete($testimonial))
                return redirect('/admin/testimonial')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/testimonial')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }
    }
}
