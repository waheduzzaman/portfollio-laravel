<?php

namespace App\Http\Controllers;

use App\Http\Requests\AboutMeRequest;
use App\Models\AboutMe;
use App\Services\AboutMeService;
use Illuminate\Http\Request;

class AboutMeController extends Controller
{

    private AboutMeService $aboutMeService;

    public function __construct()
    {
        $this->aboutMeService = new AboutMeService();
    }



    public function index()
    {
        return view('admin.about_me.create-update-about-me')->with("aboutMe", AboutMe::find(1))->with("navSelected", 2);
    }



    public function store(AboutMeRequest $request)
    {
        if ($this->aboutMeService->createOrUpdate($request))
            return redirect('/admin/about')->with("successMessage", "Updated About Me!");
        else
            return redirect('/admin/about')->with("errorMessage", "Error while updating About Me!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AboutMe  $aboutMe
     * @return \Illuminate\Http\Response
     */
    public function show(AboutMe $aboutMe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutMe  $aboutMe
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutMe $aboutMe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AboutMe  $aboutMe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AboutMe $aboutMe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AboutMe  $aboutMe
     * @return \Illuminate\Http\Response
     */
    public function destroy(AboutMe $aboutMe)
    {
        //
    }
}
