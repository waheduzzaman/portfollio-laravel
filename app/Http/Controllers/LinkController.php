<?php

namespace App\Http\Controllers;

use App\Exceptions\ClassCastException;
use App\Http\Requests\LinkRequest;
use App\Models\Link;
use App\Services\LinkService;

class LinkController extends Controller
{

    private LinkService $linkService;

    public function __construct()
    {
        $this->linkService = new LinkService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.links.linkList")->with("links", Link::all())->with("navSelected",3);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.links.createSocialLink")->with("navSelected",3);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LinkRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LinkRequest $request)
    {
        if ($this->linkService->create($request))
            return redirect('/admin/socialLinks')->with("successMessage", "Social Link created");
        else
            return redirect('/admin/socialLinks')->with("errorMessage", "Error while saving!");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Link $socialLink
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $socialLink)
    {
        return view("admin.links.editSocialLink")->with("link", $socialLink)->with("navSelected",3);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LinkRequest $request
     * @param \App\Models\Link $socialLink
     * @return \Illuminate\Http\Response
     */
    public function update(LinkRequest $request, Link $socialLink)
    {
        if ($this->linkService->update($socialLink, $request)) {
            return redirect('/admin/socialLinks')->with("successMessage", "Successfully Updated!");
        } else
            return redirect('/admin/socialLinks')->with("errorMessage", "Update Error!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Link $socialLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $socialLink)
    {
        try {
            if ($this->linkService->delete($socialLink))
                return redirect('/admin/socialLinks')->with("successMessage", "Successfully Deleted!");
            else
                return redirect('/admin/socialLinks')->with("errorMessage", "Delete Error!");
        } catch (ClassCastException $e) {
            abort(404, $e->getMessage());
        }

    }
}
