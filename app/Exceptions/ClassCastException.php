<?php

namespace App\Exceptions;

use Exception;

class ClassCastException extends Exception
{
    public function render($request,$exception){
        return "Class Type did not match!";
    }
}
