<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    protected $fillable = [
        "organization_fk",
        "text",
        'project_name',
        'url',
        'period',
    ];

    public function organization(){
        return $this->belongsTo(Organization::class, 'organization_fk');
    }
}
