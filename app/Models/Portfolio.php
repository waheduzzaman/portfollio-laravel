<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = [
        "title",
        "project_link",
        "project_date",
        "description",
        "portfolio_type_fk",
        "image_urls",
    ];

    function getImages()
    {
        return json_decode($this->image_urls);
    }

    function getType()
    {
        return $this->belongsTo(PortfolioType::class, 'portfolio_type_fk');
    }
}
