<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutMe extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "designation",
        "punchline",
        "short_description",
        "main_bg_image",
        "about_image",
        "current_org",
    ];
}
