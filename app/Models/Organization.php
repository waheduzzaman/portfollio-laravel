<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;

    protected $fillable = [
        "short_description",
        "started_at",
        "ended_at",
        "company_name",
        "website",
        "address",
    ];
}
