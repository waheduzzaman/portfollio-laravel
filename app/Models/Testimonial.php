<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasFactory;

    protected $fillable = [
        "client_name",
        "client_photo",
        "client_designation",
        "client_organization_name",
        "client_organization_link",
        "testaments",
    ];
}
