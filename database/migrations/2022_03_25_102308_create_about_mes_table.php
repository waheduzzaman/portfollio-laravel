<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_mes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name",40);
            $table->string("designation",100);
            $table->string("short_description",1000);
            $table->string("punchline",450)->nullable();
            $table->string("main_bg_image",190);
            $table->string("about_image",290);
            $table->string("current_org",120);
        });

        \Illuminate\Support\Facades\DB::table("about_mes")->insert([
            "name" => 'Md. Waheduzzaman', //change this to your preferred name
            "designation" => 'Software Engineer', //change this to your own designation
            "short_description" => 'Passionate Software Engineer', //change this too
            "punchline" => 'Anything intereseting about you in a sentence', //change this too
            "current_org" => 'dgMarket Int. Inc.', //change this as well
            "main_bg_image" => '/img/bg.jpeg', //change this from admin panel
            "about_image" => '/img/me.jpg', //change this from admin panel
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_mes');
    }
};
