<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger("portfolio_type_fk");
            $table->string("project_date")->nullable();
            $table->string("image_urls",400);
            $table->string("title");
            $table->string("description");
            $table->string("project_link");

            $table->foreign("portfolio_type_fk")->references("id")->on("portfolio_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
};
