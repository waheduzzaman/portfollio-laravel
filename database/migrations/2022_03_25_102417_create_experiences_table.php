<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger("organization_fk")->nullable();
            $table->string('project_name', 120);
            $table->string('url', 350)->nullable();
            $table->string("text", 290);
            $table->string("period",40)->default("present");

            $table->foreign("organization_fk")->on("organizations")->references("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
};
