//todo:: change the entire file upload mechanism to onPage background rest api call everywhere.

function addFileChangeListener(selector, output, isMultiple = false) {
    selector.addEventListener("change", (e) => {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = e.target.files;
            for (let i = 0; i < files.length; i++) {
                if (!files[i].type.match("image")) continue;
                const picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    const picFile = event.target;
                    if (isMultiple) {
                        // const outputNode = document.querySelector("#"+output);
                        // const div = document.createElement("span");
                        // div.innerHTML = `<div class="container-img"> <img class="w-100 border-radius-lg shadow-sm p-1 avatar avatar-xl" src="${picFile.result}" title="${picFile.name}"/><a class="p-2" onclick="cancelClicked(${i});"><i class="fa fa-remove"></i></span></div>`;
                        // outputNode.appendChild(div);
                        $("#result").append(`<div id=preload_${i}  class="container-img"> <img class="w-100 border-radius-lg shadow-sm p-1 avatar avatar-xl" src="${picFile.result}"/><a class="p-2" onclick=deleteFile(${i})><i class="fa fa-remove"></i></span></div>`);
                    }
                    else {
                        const inHtml = `<img class="w-100 border-radius-lg shadow-sm p-1" src="${picFile.result}" title="${picFile.name}"/>`;
                        document.getElementById(output).innerHTML = inHtml;
                    }
                })
                picReader.readAsDataURL(files[i]);
            }
        } else {
            console.log("no support for File api");
        }
    });
}


function deleteFile(index) {
    filelistall = $('#images').prop("files");
    var fileBuffer = [];
    Array.prototype.push.apply(fileBuffer, filelistall);
    fileBuffer.splice(index, 1);
    const dT = new ClipboardEvent('').clipboardData || new DataTransfer();
    for (let file of fileBuffer) { dT.items.add(file); }
    filelistall = $('#images').prop("files", dT.files);
    $("#preload_" + index).remove()
}