<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: me@waheduzzaman.com
 * Date: 22/3/22
 * Time: 6:15 PM
 */
?>
<!-- ======= Resume Section ======= -->
<section id="resume" class="resume">
    <div class="container">

        <div class="section-title">
            <h2>Experiences</h2>
            <p>My Experience Timeline</p>
        </div>

        <div class="row">
            <div class="col-lg-9">
                @foreach ($experiences as $experience)
                <div class="resume-item">
                    <h4>
                        @if ($experience->organization)
                        {{$experience->organization->company_name}}
                        @endif
                    </h4>
                    <h5>@if ($experience->period)
                        {{$experience->period}}
                        @else
                        Present
                        @endif</h5>
                    <ul>
                        <li><strong>Title</strong>: <em class="text-primary">{{$experience->project_name}}</em></li>
                        @if ($experience->url)
                        <li><strong>URL</strong>: <a target="blank" href='{{$experience->url}}'>{{$experience->url}}</a>
                        </li>
                        @endif

                    </ul>
                    <p>{{$experience->text}}</p>
                </div>
                @endforeach
            </div>
            <div class="col-lg-2">
                @foreach ($organizations as $organization)
                <div class="resume-item">
                    <h4><a target="blank" href="{{$organization->website}}">{{$organization->company_name}}</a></h4>
                    <h5>@if ($organization->ended_at && $organization->ended_at)
                        {{$organization->started_at}} - {{$organization->ended_at}}
                        @else
                        Present
                        @endif</h5>
                    <p>{{$organization->short_description}}</p>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</section><!-- End Resume Section -->