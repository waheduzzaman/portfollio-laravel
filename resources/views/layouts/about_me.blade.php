<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: me@waheduzzaman.com
 * Date: 22/3/22
 * Time: 6:14 PM
 */
?>
<!-- ======= About Section ======= -->
<section id="about" class="about">

    <!-- ======= About Me ======= -->
    <div class="about-me container">

        <div class="section-title">
            <h2>About</h2>
            <p>Learn more about me</p>
        </div>

        <div class="row">
            <div class="col-lg-4" data-aos="fade-right">
                <img src="{{ asset($aboutMe->about_image) }}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
                <h3>{{$aboutMe->designation}}</h3>
                <p class="fst-italic">
                </p>
                @if (count($bios)>0)
                <div class="row">
                    @foreach ($bios as $bio)
                    <div class="col-lg-6">
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <strong>{{$bio->key}}:</strong>
                                <span>{{$bio->value}}</span>
                            </li>
                        </ul>
                    </div>
                    @endforeach
                </div>
                @endif

                <p>
                    {{$aboutMe->short_description}}
                </p>
            </div>
        </div>

    </div><!-- End About Me -->


    <!-- ======= Skills  ======= -->
    @if (count($skills)>0)
    <div class="skills container">

        <div class="section-title">
            <h2>Skills</h2>
        </div>

        <div class="row skills-content">
            @foreach ($skills as $skill)

            <div class="col-lg-6">
                <div class="progress">
                    <span class="skill">{{$skill->key}} <i class="val"></i></span>
                    <div class="progress-bar-wrap">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$skill->value}}" aria-valuemin="0"
                            aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    <!-- End Skills -->



    <!-- ======= Testimonials ======= -->
    @if (count($testimonials)>0)
    <div class="testimonials container">

        <div class="section-title">
            <h2>Testimonials</h2>
        </div>

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
            <div class="swiper-wrapper">

                @foreach ($testimonials as $testimonial)
                <div class="swiper-slide">
                    <div class="testimonial-item">
                        <p>
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            {{$testimonial->testaments}}
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                        <img src="{{$testimonial->client_photo}}" class="testimonial-img" alt="">
                        <h3>{{$testimonial->client_name}}</h3>
                        <h4>{{$testimonial->client_designation}} at {{$testimonial->client_organization_name}}</h4>
                        @if ($testimonial->client_organization_link)
                        <h4><a href="{{$testimonial->client_organization_link}}">Website</a></h4>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>

    </div>
    @endif
    <!-- End Testimonials  -->

</section><!-- End About Section -->