<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/3/22
 * Time: 6:19 PM
 */
?>
<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="section-title">
            <h2>Portfolio</h2>
            <p>My Works</p>
        </div>

        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    @foreach ($portfolioTypes as $portfolioType)
                    <li data-filter=".filter-{{$portfolioType->id}}" >{{$portfolioType->title}}</li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row portfolio-container">
            @foreach ($portfolios as $portfolio)
            <div class="col-lg-4 col-md-6 portfolio-item filter-{{$portfolio->getType->id}}">
                <div class="portfolio-wrap">
                    <img src="{{$portfolio->getImages()[0]}}" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4>{{$portfolio->title}}</h4>
                        <p>{{$portfolio->getType->title}}</p>
                        <div class="portfolio-links">
                            <a href="{{url('portfolio/'.$portfolio->id)}}" data-gallery="portfolioDetailsGallery"
                                data-glightbox="type: external" class="portfolio-details-lightbox mt-2"
                                title="Portfolio Details"><i class="bi bi-arrows-fullscreen"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section><!-- End Portfolio Section -->