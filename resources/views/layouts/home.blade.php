<?php
/**
 * Created by IntelliJ IDEA
 * USER: MD. WAHEDUZZAMAN
 * EMAIL: hi@waheduzzaman.com
 * Date: 22/3/22
 * Time: 7:23 PM
 */
?>
<style>
    body::before {
        content: "";
        position: fixed;
        background: #040404 url({{ asset($aboutMe->main_bg_image) }}) top right no-repeat;
        background-size: cover;
        left: 0;
        right: 0;
        top: 0;
        height: 100vh;
        z-index: -1;
    }
</style>
<header id="header">
    <div class="container">

        <h1><a href="index.html">{{$aboutMe->name}}</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        {{-- <a href="index.html" class="mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a> --}}
        
        <h2><span>{{$aboutMe->designation}}</span> at {{$aboutMe->current_org}}</h2>
        <h2>{{$aboutMe->punchline}}</h2>
        

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link active" href="#header">Home</a></li>
                <li><a class="nav-link" href="#about">About</a></li>
                <li><a class="nav-link" href="#resume">Experiences</a></li>
                <li><a class="nav-link" href="#portfolio">Portfolio</a></li>
                <li><a target="blank" class="nav-link" href="https://blog.waheduzzaman.com">Blog</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        <div class="social-links">
            @foreach($links as $link)
            <a target="_blank" href="{{$link->url}}"><i class="bi {{$link->bootstrap_icon_name}}"></i></a>
            @endforeach
        </div>

    </div>
</header><!-- End Header -->