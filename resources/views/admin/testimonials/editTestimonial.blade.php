<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 9:22 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Update Testimonial</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/testimonial/'.$testimonial->id)}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input hidden name="id" value="{{$testimonial->id}}">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Name</label>
                                <input value="{{$testimonial->client_name}}" required name="client_name"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Designation</label>
                                <input value="{{$testimonial->client_designation}}" required name="client_designation"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Company Name</label>
                                <input value="{{$testimonial->client_organization_name}}" required
                                    name="client_organization_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Website Link</label>
                                <input type="url" value="{{$testimonial->client_organization_link}}" required
                                    name="client_organization_link" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client's Words</label>
                                <input value="{{$testimonial->testaments}}" required name="testaments"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <table>
                                <input name="client_img" type="file" style="display:none" id="client_img" />
                                <tbody class="text-center">
                                    <tr>
                                        <td>
                                            <div id="result" class="avatar avatar-xxl position-relative mt-3">
                                                <img src="{{$testimonial->client_photo}}" alt="client_img"
                                                    class="w-100 border-radius-lg shadow-sm">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="button" class="btn btn-outline-success btn-sm mb-0 mt-2"
                                                onclick="document.getElementById('client_img').click()">
                                                Client Image
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right mt-2">Update Testimonial</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/fileUpload.js') }}"></script>
<script>
    addFileChangeListener(document.querySelector("#client_img"),"result");
</script>
@endsection