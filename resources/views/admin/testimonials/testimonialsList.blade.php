<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 30/07/2021
 * Time: 2:54 PM
 */ ?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg p-3">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0 text-white text-capitalize">My Testimonials</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a class="btn bg-gradient-dark mb-0" href="{{ route('testimonial.create')}}">
                                <i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Client Photo
                            </th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Client
                            </th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Client Details
                            </th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Testimonials
                            </th>
                            <th class="text-secondary opacity-7"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($testimonials as $testimonial)
                        <tr>
                            <td>
                                <p class="text-xs"> {{$testimonial->id}}</p>
                            </td>
                            <td>
                                <div class="avatar avatar-xl position-relative mt-2">
                                    <img src="{{$testimonial->client_photo}}" alt="client_img" class="w-100 border-radius-lg shadow-sm">
                                </div>
                            </td>
                            <td>
                                <p class="text-xs"> {{$testimonial->client_name}}</p>
                                <p class="text-xs"> {{$testimonial->client_designation}}</p>
                            </td>
                            <td>
                                <p class="text-xs"> {{$testimonial->client_organization_name}}</p>
                                <p class="text-xs"> {{$testimonial->client_organization_link}}</p>
                            </td>
                            <td>
                                <p class="text-xs"> {{$testimonial->testaments}}</p>
                            </td>

                            <td class="align-middle">
                                <a href="{{url('admin/testimonial/'.$testimonial->id.'/edit')}}"
                                   class="font-weight-bold text-xs btn btn-info">
                                    Edit
                                </a>

                                <form action="{{ route('testimonial.destroy', $testimonial->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger font-weight-bold text-xs" type="submit" onclick="return confirm('Confirm delete this record?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

