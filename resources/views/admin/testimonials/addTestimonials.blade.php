<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Add New Testimonial</h4>

            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/testimonial')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Name</label>
                                <input required name="client_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Designation</label>
                                <input required name="client_designation" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Company Name</label>
                                <input required name="client_organization_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client Website Link</label>
                                <input type="url" required name="client_organization_link" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Client's Words</label>
                                <input required name="testaments" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <table>
                                <input accept="image/jpeg, image/gif, image/jpg, image/png" name="client_img" type="file" style="display:none" id="client_img"/>
                                <tbody class="text-center">
                                <tr>
                                    <td>
                                        <div id="result" class="avatar avatar-xxl position-relative">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button type="button" class="btn btn-outline-success btn-sm mb-0 mt-2" onclick="document.getElementById('client_img').click()">
                                            Client Image
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">Add</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/fileUpload.js') }}"></script>
<script>
    addFileChangeListener(document.querySelector("#client_img"),"result");
</script>
@endsection