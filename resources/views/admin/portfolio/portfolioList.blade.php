<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 24/07/2022
 * Time: 3:19 PM
 */ ?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg p-3">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0 text-white text-capitalize">My Portfolios</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a class="btn bg-gradient-dark mb-0" href="{{url('admin/portfolio/create')}}">
                                <i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Portfolio
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Title
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Project Link
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Date
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Description
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Portfolio Type
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Images
                                </th>
                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($portfolios as $portfolio)
                            <tr>
                                <td>
                                    <p class="text-xs"> {{$portfolio->id}}</p>
                                </td>
                                <td>
                                    <p class="text-xs"> {{$portfolio->title}}</p>
                                </td>
                                <td>
                                    <p class="text-xs"> {{$portfolio->project_link}}</p>
                                </td>
                                <td>
                                    <p class="text-xs"> {{$portfolio->project_date}}</p>
                                </td>
                                <td>
                                    <p class="text-xs"> {{$portfolio->description}}</p>
                                </td>
                                <td>
                                    <p class="text-xs"> {{$portfolio->portfolio_type_fk}}</p>
                                </td>
                                <td>
                                    @foreach ($portfolio->getImages() as $image )
                                    <div class="avatar avatar-xl position-relative mt-2">
                                        <img src="{{$image}}" alt="client_img" class="w-100 border-radius-lg shadow-sm">
                                    </div>
                                    @endforeach
                                </td>

                                <td class="align-middle">
                                    <a href="{{url('admin/portfolio/'.$portfolio->id.'/edit')}}"
                                        class="font-weight-bold text-xs btn btn-info">
                                        Edit
                                    </a>

                                    <form action="{{ route('portfolio.destroy', $portfolio->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger font-weight-bold text-xs" type="submit"
                                            onclick="return confirm('Confirm delete this record?')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection