<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 9/7/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Add New Portfolio</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/portfolio')}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Title</label>
                                <input required name="title" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project Link</label>
                                <input required name="project_link" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project Date (ex: 01 March, 2020)</label>
                                <input required name="project_date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class=" row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project Details</label>
                                <input required name="description" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class=" row">
                        <div class="col-md-2">
                            <select name="portfolio_type_fk" id="portfolio_type_fk"
                                class="form-control form-control-md btn bg-gradient-secondary ">
                                <option class="dropdown-item border-radius-md">Select Type</option>
                                @foreach ($types as $type)
                                <option value="{{$type->id}}">{{$type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <input name="images[]" accept="image/jpeg, image/png, image/jpg, image/gif" type="file" multiple="multiple"
                            style="display:none" id="images" />
                        <div class="col-md-12">
                            <div id="result" class="position-relative">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-outline-success btn-sm mb-0 mt-2"
                        onclick="document.getElementById('images').click()">Images
                    </button>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">ADD</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/fileUpload.js') }}"></script>
<script>
    addFileChangeListener(document.querySelector("#images"),"result",true);
</script>
@endsection