<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 9/7/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Update Experience</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/experience/'.$experience->id)}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <input hidden value="{{$experience->id}}" name="id"/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project Name</label>
                                <input value="{{$experience->project_name}}" required name="project_name" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Period (ex: 2021 or empty for present)</label>
                                <input value="{{$experience->period}}" name="period" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project URL</label>
                                <input value="{{$experience->url}}" name="url" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class=" col-md-4">
                            <select name="organization_fk" id="organization_fk"
                                class="form-control form-control-md btn bg-gradient-secondary ">
                                <option class="dropdown-item border-radius-md">Select Organization</option>
                                @foreach ($organizations as $organization)
                                <option {{($organization->id == $experience->organization_fk)? 'selected':''}} value="{{$organization->id}}">{{$organization->company_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class=" row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Project Details</label>
                                <input value="{{$experience->text}}" required name="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">UPDATE</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection