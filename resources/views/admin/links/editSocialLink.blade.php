<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 4/07/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Social Link</h4>
                    <p class="card-category">For bootstrap icon reference, <a href="https://icons.getbootstrap.com">CLICK HERE</a></p>
                </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/socialLinks/'.$link->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <input hidden name="id" value="{{$link->id}}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Social Site Name (ex: Facebook, Twitter)</label>
                                    <input value="{{$link->title}}" required name="title" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Icon Name(bootstrap icon. ex: bi-facebook or bi-twitter)</label>
                                    <input value="{{$link->bootstrap_icon_name}}" required name="bootstrap_icon_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Your Social Site Link (your profile link)</label>
                                    <input value="{{$link->url}}" required name="url" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Update Link</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
