<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 4/07/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Portfolio Type</h4>
                    <p class="card-category">Ex: Case Study, Mobile App, Website, Web App, Design etc </p>
                 </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/portfolioType/'.$portfolioType->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <input hidden name="id" value="{{$portfolioType->id}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Portfolio Type Title</label>
                                    <input value="{{$portfolioType->title}}" required name="title" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Update Portfolio Type</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
