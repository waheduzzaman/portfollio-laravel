<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 2:05 AM
 */ ?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link rel="stylesheet" href="{{ asset('admin/css/nucleo-icons.css?v=2.1.2') }}">
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" rel="stylesheet" href="{{ asset('admin/css/material-dashboard.css?v=3.0.0') }}">
    <!--import  bootstrap icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="g-sidenav-show  bg-gray-200">
    <aside
        class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark"
        id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
                aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="http://www.waheduzzaman.com" target="_blank">
                <span class="ms-1 font-weight-bold text-white">My Portfolio Admin Panel</span>
            </a>
        </div>
        <hr class="horizontal light mt-0 mb-2">
        <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 1 ? 'active bg-gradient-primary' : '' }} nav-link text-white"
                        href="{{url("/admin/home")}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">dashboard</i>
                        </div>
                        <span class="nav-link-text ms-1">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 2 ? 'active bg-gradient-primary' : '' }} nav-link text-white"
                        href="{{url('/admin/about')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">person</i>
                        </div>
                        <span class="nav-link-text ms-1">About Me</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 3 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('admin/socialLinks')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">share</i>
                        </div>
                        <span class="nav-link-text ms-1">Social Links</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 4 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('admin/skills')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">bolt</i>
                        </div>
                        <span class="nav-link-text ms-1">Skills</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 5 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('/admin/testimonial/')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">upcoming</i>
                        </div>
                        <span class="nav-link-text ms-1">Testimonials</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 6 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('admin/bio')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">assignment_ind</i>
                        </div>
                        <span class="nav-link-text ms-1">Bio</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 7 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('/admin/organization/')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">receipt_long</i>
                        </div>
                        <span class="nav-link-text ms-1">Orgaizations</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 10 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('/admin/experience/')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">receipt_long</i>
                        </div>
                        <span class="nav-link-text ms-1">Experiences</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 8 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('/admin/portfolioType')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">splitscreen</i>
                        </div>
                        <span class="nav-link-text ms-1">Portfolio Type</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="{{ isset($navSelected) &&  $navSelected == 9 ? 'active bg-gradient-primary' : '' }} nav-link text-white "
                        href="{{url('/admin/portfolio')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">splitscreen</i>
                        </div>
                        <span class="nav-link-text ms-1">Portfolios</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white " href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">logout</i>
                        </div>
                        <span class="nav-link-text ms-1">Sign out</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
            navbar-scroll="true">
            <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">
                    <h6 class="font-weight-bolder mb-0"></h6>
                </nav>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="container-fluid py-4">
            @yield('content')

            <footer class="footer py-4  ">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-lg-between">
                        <div class="col-lg-6 mb-lg-0 mb-4">
                            <div class="copyright text-center text-sm text-muted text-lg-start">
                                <a href="https://www.waheduzzaman.com">
                                    waheduzzaman
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </main>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>

    
    <!--   Core JS Files   -->
    <script src="{{ asset('admin/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('admin/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/plugins/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('admin/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('admin/js/plugins/smooth-scrollbar.min.js') }}"></script>
    
    
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
        </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('admin/js/material-dashboard.min.js?v=3.0.0') }}"></script>
    
    <script>
        function showNotificationMessage(type, message) {
            $.notify({
                icon: "add_alert",
                message: message
                
            }, {
                type: type,
                timer: 3000,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
        }
        </script>

@foreach ($errors->all() as $error)
{{--
    <li>{{ $error }}</li>
    --}}
    <script>
        showNotificationMessage('danger', '{{ $error }}')
        </script>
    @endforeach
    
    @if(session()->get('successMessage') != null)
    <script>
        showNotificationMessage('success', "{{session()->get('successMessage') }}")
        </script>
    @endif
    
    @if(session()->get('errorMessage') != null)
    <script>
        showNotificationMessage('danger', "{{session()->get('errorMessage') }}")
        </script>
    @endif
    
    @if(session()->get('infoMessage') != null)
    <script>
        showNotificationMessage('info', "{{session()->get('infoMessage') }}")
        </script>
    @endif
    
    @yield('scripts')
</body>

</html>