<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 8:02 PM
 */ ?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">

                <div class="bg-gradient-primary shadow-primary border-radius-lg p-3">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0 text-white text-capitalize">Organization List</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a class="btn bg-gradient-dark mb-0" href="{{url('/admin/organization/create')}}">
                                <i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Organization
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">id</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Organization Name / Website
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    About
                                </th>

                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Period
                                </th>
                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($organizations as $organization)
                            <tr>
                                <td>
                                    <p class="text-xs"> {{$organization->id}}</p>
                                </td>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{$organization->company_name}}</h6>
                                            <h7 class="mb-0 text-sm"><a target="blank"
                                                    href="{{$organization->website}}">{{$organization->website}}</a>
                                            </h7>
                                            <h7 class="mb-0 text-sm">{{$organization->address}}</h7>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0">{{$organization->short_description}}</p>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0">
                                        @if ($organization->ended_at && $organization->ended_at)
                                        {{$organization->started_at}} - {{$organization->ended_at}}
                                        @else
                                        Present
                                        @endif
                                    </p>
                                </td>
                                <td class="align-middle">
                                    <a href="{{url('admin/organization/'.$organization->id.'/edit')}}"
                                        class="font-weight-bold text-xs btn btn-info">
                                        Edit
                                    </a>

                                    <form action="{{ route('organization.destroy', $organization->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger font-weight-bold text-xs" type="submit"
                                            onclick="return confirm('Confirm delete this record?')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection