<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 9/7/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Add New Organization</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/organization')}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Name</label>
                                <input required name="company_name" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Address</label>
                                <input required name="address" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Website</label>
                                <input required name="website" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Started At (ex: 2013)</label>
                                <input required name="started_at" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Left At (ex: 2014)</label>
                                <input name="ended_at" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class=" row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Company Details</label>
                                <input required name="short_description" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">ADD</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection