<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 9/7/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Update Organization</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/organization/'.$organization->id)}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <input hidden value="{{$organization->id}}" name="id"/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Name</label>
                                <input value="{{$organization->company_name}}" required name="company_name" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Address</label>
                                <input value="{{$organization->address}}" required name="address" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Website</label>
                                <input value="{{$organization->website}}" required name="website" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Started At (ex: 2013)</label>
                                <input value="{{$organization->started_at}}" required name="started_at" class="form-control">
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Left At (ex: 2014)</label>
                                <input value="{{$organization->ended_at}}" name="ended_at" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class=" row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Company Details</label>
                                <input value="{{$organization->short_description}}" required name="short_description" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">UPDATE</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection