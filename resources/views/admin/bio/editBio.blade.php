<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 9:22 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Bio Entry</h4>
                </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/bio/'.$bio->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <input hidden name="id" value="{{$bio->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Title (ex: Birthday, Age, Website)</label>
                                    <input value="{{$bio->key}}" required name="key" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">value (ex: 1 Jan 1901, 32, https://waheuzzaman.com)</label>
                                    <input value="{{$bio->value}}" required name="value" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Update Bio Entry</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
