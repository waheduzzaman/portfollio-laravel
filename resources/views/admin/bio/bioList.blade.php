<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 8:02 PM
 */ ?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg p-3">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0 text-white text-capitalize">My Biodata</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a class="btn bg-gradient-dark mb-0" href="{{url('admin/bio/create')}}">
                                <i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Bio Entry
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Title
                            </th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Value
                            </th>
                            <th class="text-secondary opacity-7"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($bios as $bio)
                        <tr>
                            <td>
                                <p class="text-xs"> {{$bio->id}}</p>
                            </td>
                            <td>
                                <p class="text-xs"> {{$bio->key}}</p>
                            </td>
                            <td>
                                <p class="text-xs"> {{$bio->value}}</p>
                            </td>

                            <td class="align-middle">
                                <a href="{{url('admin/bio/'.$bio->id.'/edit')}}"
                                   class="font-weight-bold text-xs btn btn-info">
                                    Edit
                                </a>

                                <form action="{{ route('bio.destroy', $bio->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger font-weight-bold text-xs" type="submit" onclick="return confirm('Confirm delete this record?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

