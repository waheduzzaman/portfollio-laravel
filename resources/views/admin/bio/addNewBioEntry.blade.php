<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 5/07/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add New Entry For Bio</h4>
                    <p class="card-category">Add new bio entry. ex: Birthday : 1 Jan 1901</p>
                </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/bio')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Title (ex: Birthday, Age, Website)</label>
                                    <input required name="key" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">value (ex: 1 Jan 1901, 32, https://waheuzzaman.com)</label>
                                    <input required name="value" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add To Bio</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
