<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: hi@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add New Skill</h4>
                    <p class="card-category">Example: Laravel - 90</p>
                </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/skills')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Skill Title (ex: Java, Spring-Boot, Flutter)</label>
                                    <input required name="key" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Parcentage (60, 80, 95, 100)</label>
                                    <input required name="value" class="form-control" type="number">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Skill</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
