<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 3/12/2021
 * Time: 9:22 PM
 */
?>
@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Skills</h4>
                    <p class="card-category">Example: Laravel - 90</p>
                </div>
                <div class="card-body">
                    <form class="text-start" action="{{url('admin/skills/'.$skill->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <input hidden name="id" value="{{$skill->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Skill Title (ex: Java, Spring-Boot, Flutter)</label>
                                    <input value="{{$skill->key}}" required name="key" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Parcentage (60, 80, 95, 100)</label>
                                    <input value="{{$skill->value}}" required name="value" class="form-control" type="number">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Update Skill</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
