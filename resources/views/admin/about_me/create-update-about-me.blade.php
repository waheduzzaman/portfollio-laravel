<?php
/**
 * Created by IntelliJ IDEA.
 * User: Md. Waheduzaaman
 * Email: me@waheduzzaman.com
 * Date: 8/07/2022
 * Time: 9:18 PM
 */
?>
@extends('admin.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">About Me</h4>
            </div>
            <div class="card-body">
                <form class="text-start" action="{{url('admin/about')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input name="id" hidden value="{{$aboutMe->id ?? ''}}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Full Name</label>
                                <input required name="name" class="form-control" value="{{$aboutMe->name??''}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Designation</label>
                                <input required name="designation" class="form-control"
                                    value="{{$aboutMe->designation??''}}">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Current Organization</label>
                                <input required name="current_org" class="form-control"
                                    value="{{$aboutMe->current_org??''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Your Punchline!</label>
                                <input required name="punchline" class="form-control"
                                    value="{{$aboutMe->punchline??''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group input-group-outline my-3">
                                <label class="form-label">Short description about you</label>
                                <input required name="short_description" class="form-control"
                                    value="{{$aboutMe->short_description??''}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <table>
                                <input name="main_bg_img" accept="image/jpeg, image/png, image/jpg, image/gif"
                                    type="file" style="display:none" id="main_bg_img" />
                                <tbody class="text-center">
                                    <tr>
                                        <td>
                                            <div id="result-main_bg_img" class="avatar avatar-xxl position-relative">
                                                <img src="{{ asset($aboutMe->main_bg_image) }}" alt="profile_image"
                                                    class="w-100 border-radius-lg shadow-sm">
                                            </div>
                                        </td>
                                        {{-- <td><img src="{{ asset($aboutMe->main_bg_image) }}" width="100"
                                                height="100"></td> --}}
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="button" class="btn btn-outline-primary btn-sm mb-0 mt-2"
                                                onclick="document.getElementById('main_bg_img').click()">Main
                                                Page
                                                Image
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-2">
                            <table>
                                <input name="about_img" type="file" style="display:none" id="about_img" />
                                <tbody class="text-center">
                                    <tr>
                                        <td>
                                            <div id="result-about-img" class="avatar avatar-xxl position-relative">
                                                <img src="{{ asset($aboutMe->about_image) }}" alt="profile_image"
                                                    class="w-100 border-radius-lg shadow-sm">
                                            </div>
                                        </td>
                                        {{-- <td><img src="{{ asset($aboutMe->about_image) }}" width="100" height="100">
                                        </td> --}}
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="button" class="btn btn-outline-success btn-sm mb-0 mt-2"
                                                onclick="document.getElementById('about_img').click()">About
                                                Page
                                                Image
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="row">
                        <div class="text-end">
                            <button type="submit" class="col-md-2 mt-4 btn btn-primary">Save</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/fileUpload.js') }}"></script>
<script>
    addFileChangeListener(document.querySelector("#main_bg_img"),"result-main_bg_img");
    addFileChangeListener(document.querySelector("#about_img"),"result-about-img");
</script>
@endsection